$(function () {


   /*轮播一*/

    // var banner_index = 0;
    // $('.banner_box ul li').eq(banner_index).show().siblings().hide();
    //
    // setInterval(function(){
    //     banner_index++;
    //     if(banner_index >= $('.banner_box ul li').length){
    //         banner_index = 0;
    //     }
    //     $('.banner_box ul li').eq(banner_index).fadeIn().siblings().hide();
    // }, 2000);


    /*轮播二  加入小数点查看当前显示位置*/
    // var banner_index = 0;
    // $('.banner_box ul li').eq(banner_index).show().siblings().hide();
    //
    // setInterval(function(){
    //     banner_index++;
    //     if(banner_index >= $('.banner_box ul li').length){
    //         banner_index = 0;
    //     }
    //     $('.banner_box ul li').eq(banner_index).fadeIn().siblings().hide();
    //     $('.banner_box div span').eq(banner_index).addClass('active').siblings().removeClass('active');
    //
    // }, 2000);

    /*轮播三  点击小数点跳转到相应图片*/

    // var banner_index = 0;
    // var dot_index = 0;
    //
    // $('.banner_box ul li').eq(banner_index).show().siblings().hide();
    // $('.banner_box div span').eq(banner_index).addClass('active').siblings().removeClass('active');
    //
    // var timer = setInterval(function(){
    //     banner_index++;
    //     if(banner_index >= $('.banner_box ul li').length){
    //         banner_index = 0;
    //     }
    //     $('.banner_box ul li').eq(banner_index).fadeIn().siblings().hide();
    //     $('.banner_box div span').eq(banner_index).addClass('active').siblings().removeClass('active');
    //
    // }, 6000);
    //
    // $('.banner_box div span').click(function(){
    //     dot_index = $('.banner_box div span').index(this);
    //     banner_index = dot_index;
    //     clearInterval(timer);
    //     $('.banner_box ul li').eq(banner_index).show().siblings().hide();
    //     $('.banner_box div span').eq(banner_index).addClass('active').siblings().removeClass('active');
    //
    //     timer = setInterval(function(){
    //         banner_index++;
    //         if(banner_index >= $('.banner_box ul li').length){
    //             banner_index = 0;
    //         }
    //         $('.banner_box ul li').eq(banner_index).fadeIn().siblings().hide();
    //         $('.banner_box div span').eq(banner_index).addClass('active').siblings().removeClass('active');
    //
    //     }, 2000);
    //
    // })

    /*轮播四  减小代码量*/
    var banner_index = 0;
    var dot_index = 0;
    var timer;
    startShow();
    startTimer();

    $('.banner_box div span').click(function(){
        dot_index = $('.banner_box div span').index(this);
        banner_index = dot_index;
        clearInterval(timer);
        startShow();
        startTimer();
    });

    function startShow(){
        $('.banner_box ul li').eq(banner_index).fadeIn().siblings().hide();
        $('.banner_box div span').eq(banner_index).addClass('active').siblings().removeClass('active');
    };

    function startTimer(){
        timer = setInterval(function(){
            banner_index++;
            if(banner_index >= $('.banner_box ul li').length){
                banner_index = 0;
            }
            startShow();

        }, 2000);
    };
});